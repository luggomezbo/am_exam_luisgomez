import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
from scipy.stats import norm

def normalize_sp(frequency, spectrum, deg):
    
    # Fit a polynomial curve to the continuum of the spectrum
    coeffs = np.polyfit(frequency, spectrum, deg)
    continuum = np.polyval(coeffs, frequency)
    # Divide the spectrum by the continuum to obtain a normalized spectrum
    return spectrum / continuum

def plot_lines_nofit(wl_range, sp_lines_list):

    # plotting the second spectra provided as it is
    plt.figure(figsize=(30, 8))
    for i, line in enumerate(sp_lines_list):
        plt.plot(wl_range, line, label=f'line_{i}')
    # plt.title('Absolute spectral irradiance', fontsize = 20)
    plt.xlabel('wavelength [nm]', fontsize = 16)
    plt.ylabel('Intensity [W/m²/nm]', fontsize = 16)
    plt.legend()
    plt.grid(True)
    plt.show()
    # plt.savefig('images/plain_spectra.png', bbox_inches='tight')
    plt.clf

def plot_polyfits(wl_range, sp_line):

    # testing optimal degree of polynomial fit 
    plt.figure(figsize=(30, 8))
    for deg in range(2, 10):
        plt.plot(wl_range, normalize_sp(wl_range, sp_line, deg), label=f'{deg} degree Polynomial')
    # plt.title('Optimal polynomial degree for fitting spectra', fontsize = 20)
    plt.xlabel('wavelength', fontsize = 16)
    plt.ylabel('Amplitude', fontsize = 16)
    plt.legend()
    plt.grid(True)
    plt.show()
    # plt.savefig('images/det_polyfits.png', bbox_inches='tight')
    # plt.clf

def plot_noise(wl_range, noise, noise_dtr):

    # testing optimal degree of polynomial fit 
    plt.figure(figsize=(30, 8))
    plt.plot(wl_range, noise, label=f'Noise Signal')
    plt.plot(wl_range, noise_dtr, label='Detrended Noise Signal')
    # plt.title('Noise Signal', fontsize = 20)
    plt.xlabel('wavelength', fontsize = 16)
    plt.ylabel('Amplitude', fontsize = 16)
    plt.legend()
    plt.grid(True)
    plt.show()
    # plt.savefig('images/noise_plot.png', bbox_inches='tight')
    # plt.clf

def plot_psd(data):

    x = np.linspace(np.min(data), np.max(data), 1000)
    # Normal distribution pdf (for comparison)
    [mean_fit, std_fit] = norm.fit(data) # parameters for a normal distribution fit
    plt.figure(1, figsize=(16, 8))
    plt.hist(data, bins= 'auto', density=True, alpha=0.6, color='red', edgecolor = 'black', label= 'base data')
    plt.plot(x, norm.pdf(x, mean_fit, std_fit), color = 'blue', label = 'Normal fit')
    plt.xlim(-0.1)
    plt.title('Probability Distribution of the detrended noise signal', fontsize = 20)
    plt.xlabel('Signal values', fontsize = 16)
    plt.ylabel('Probability', fontsize = 16)
    plt.legend()
    plt.grid(True)
    plt.show()
    # plt.savefig(psd_noise.png', bbox_inches='tight')


def plot_compare_psd(base_data, detrended_data):

    plt.figure(1, figsize=(30, 8))
    plt.hist(base_data, bins= 'auto', density=True, alpha=0.6, color='red', edgecolor = 'black', label= 'base data')
    plt.hist(detrended_data, bins= 'auto', density=True, alpha=0.6, color='blue', edgecolor = 'black', label= 'detrended data')
    plt.xlim(-0.15)
    # plt.title('Probability Distribution', fontsize = 20)
    plt.xlabel('Signal values', fontsize = 14)
    plt.ylabel('Probability', fontsize = 14)
    plt.legend()
    plt.grid(True)
    plt.show()
    # plt.savefig('images/psd_comparison_noise.png', bbox_inches='tight')
    plt.clf

def plot_2xaxes(wl_range, pf_lines, deg, ticker_samples=200, ticker_wl=10):

    fig, ax1 = plt.subplots(figsize=(30,8))

    ax1.plot(range(len(pf_lines[0])), pf_lines[0], color ='red', label=f'line_1')
    ax1.set_xlabel('Sample index', fontsize=16)
    ax1.set_ylabel('Intensity [W/m²/nm]', fontsize=16)
    ax1.xaxis.set_major_locator(ticker.MultipleLocator(ticker_samples))
    ax1.tick_params(axis='y')
    ax1.grid(True, linestyle='--')
    plt.xticks(rotation=60)

    ax2 = ax1.twiny()

    for i, line in enumerate(pf_lines[1:]):
        ax2.plot(wl_range, normalize_sp(wl_range, line, deg), label=f'line_{i+2}')
    ax2.xaxis.set_ticks_position('bottom')
    ax2.xaxis.set_label_position('bottom') # set the position of the second x-axis to bottom
    ax2.spines['bottom'].set_position(('outward', 70))
    ax2.set_xlabel('Wavelength [nm]', fontsize=16)
    ax2.xaxis.set_major_locator(ticker.MultipleLocator(ticker_wl))
    ax2.grid(True, color='gray')

    fig.legend(loc='upper right')
    fig.tight_layout()
    # plt.show()
    plt.savefig('images/check_samples.png', bbox_inches='tight')
    # plt.clf

def plot_abs_lines(fl_wl_ranges, abs_lines):

    fig, axs = plt.subplots(2, 2)

    # plotting the first subplot in the top-left position
    for i, line in enumerate(abs_lines[0]):
        axs[0, 0].plot(fl_wl_ranges[0], line, label=f'line_{i+1}')
    axs[0, 0].plot(fl_wl_ranges[0], np.mean(abs_lines[0], axis=0), label='average')

    axs[0, 0].set_title('First absorbtion Line')

    # plotting the second subplot in the top-right position
    for i, line in enumerate(abs_lines[1]):
        axs[0, 1].plot(fl_wl_ranges[1], line, label=f'line_{i+1}')
    axs[0, 1].plot(fl_wl_ranges[1], np.mean(abs_lines[1], axis=0), label='average')
    axs[0, 1].set_title('Second absorbtion Line')

    # plotting the third subplot in the bottom-left position
    for i, line in enumerate(abs_lines[2]):
        axs[1, 0].plot(fl_wl_ranges[2], line, label=f'line_{i+1}')
    axs[1, 0].plot(fl_wl_ranges[2], np.mean(abs_lines[2], axis=0), label='average')
    axs[1, 0].set_title('Third absorbtion Line')

    # plotting the fourth subplot in the bottom-right position
    for i, line in enumerate(abs_lines[3]):
        axs[1, 1].plot(fl_wl_ranges[3], line, label=f'line_{i+1}')
    axs[1, 1].plot(fl_wl_ranges[3], np.mean(abs_lines[3], axis=0), label='average')
    axs[1, 1].set_title('Fourth absorbtion Line')

    for ax in axs.flat:
        ax.grid(True)
        ax.legend()
        ax.set_xlabel('Wavelength [nm]')
        ax.set_ylabel('Intensity [W/m²/nm]')

    fig.tight_layout()
    plt.show()
    # plt.savefig('images/abs_lines.png', bbox_inches='tight')
    # plt.clf