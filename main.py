import numpy as np
from scipy.stats import kurtosis
from scipy.stats import skew
from scipy.stats import norm
from scipy import signal
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
import pandas as pd

import plot_gen as pg
import functions as f

if (__name__ == '__main__'):

    # finding the filenames
    fnames = f.find_fnames()
    print(fnames)
    # reading the datasets from matlab data (turned into a csv file)
    data = f.read_data(fnames[2]) # calls 'ds_spectrum'
    sp_lines = [data[:, i] for i in range(5)] # Separate data into individual arrays
    wl_range = f.read_data(fnames[3]).flatten() # calls 'ds_wl_range', shape the np.array properly into a 1D np.array 

    # reading the datasets provided as csv files
    sp_lines_2 = f.read_data2(fnames[0]) # reads 'Sig_para_Spec_Gomez'
    wl_range2 = f.read_data2(fnames[1])[0] # reads 'Sig_para_WL_Gomez'

    print(f'The total sample range of the wavelengths are between: \n {wl_range[0]} and {wl_range[-1]} nm, meaning a range of {wl_range[-1] - wl_range[0]} nm')
    print(f'The total sample range of the intensity of the spectral lines are between: \n {np.min(sp_lines_2)} and {np.max(sp_lines_2)} W/m²/nm, meaning a range of {np.max(sp_lines_2)- np.min(sp_lines_2)} W/m²/nm')

    # pg.plot_lines_nofit(wl_range, sp_lines)
    pg.plot_lines_nofit(wl_range, sp_lines_2)

    # calculating the moments for each of the signals
    moments = []
    for i, line in enumerate(sp_lines_2):
        # calculate mean, std, kurt, skewness
        mean = line.mean()
        std = line.std()
        kurt = kurtosis(line)
        skewness = skew(line)
        moments.append([mean, std, kurt, skewness])
        print(f"the moments for line_{i+1} are:")
        print(f"{mean=} \n {std=} \n {kurt=} \n {skewness=}" )
        f.is_periodic(line)
        f.is_power_signal(line)
    
    # testing possible polynomial fits (order 2 to 10)
    pg.plot_polyfits(wl_range, sp_lines_2[0])
    # plotting the selected polynomial fit on all signals
    pf_lines_0 = [f.normalize_sp(wl_range, line, deg=4) for line in sp_lines_2]
    # pg.plot_selected_polyfit(wl_range, pf_lines_0)

    # extracting the noise from all other spectra and detrending it
    no_noise = sp_lines_2[:1]+sp_lines_2[2:]
    noise_dtr = signal.detrend(sp_lines_2[1])
    pg.plot_noise(wl_range, sp_lines_2[1], noise_dtr)

    # calculating the SNR of all other signals:
    snr_values = f.calculate_snr(sp_lines[1], no_noise)
    print(f'the SNR values of all spectral lines are: {snr_values}')


    # plotting spectra without the noise
    pf_lines = [f.normalize_sp(wl_range, line, deg=4) for line in no_noise]


    continuum = np.polyval(np.polyfit(wl_range, sp_lines_2[0], 4), wl_range)
    residuals = sp_lines_2[0] - continuum

    # plotting the PSD of the detrended noise signal and comparing it to the original noise signal
    pg.plot_psd(noise_dtr)    
    pg.plot_compare_psd(sp_lines_2[1], noise_dtr)

    fl_wl_ranges, abs_lines = f.filter_abs_lines(wl_range, pf_lines)
    # for i, line in enumerate(pf_lines):
    #     pg.plot_selected_polifit(fl_wl_ranges[i], abs_lines[i])
    #     plt.plot(fl_wl_ranges[i], np.mean(abs_lines[i], axis=0))
    #     plt.legend()
    #     plt.show
    pg.plot_abs_lines(fl_wl_ranges, abs_lines)

    peak_lines = np.array([425.0440, 426.0470, 427.1742, 428.2400])

    threshold = 0.1
    # Specify the allowable range for each wavelength
    ranges = np.array([[peak_lines[0]-threshold, peak_lines[0]+threshold], 
                       [peak_lines[1]-threshold, peak_lines[1]+threshold], 
                       [peak_lines[2]-threshold, peak_lines[2]+threshold],
                       [peak_lines[3]-threshold, peak_lines[3]+threshold]
                       ])
    print(ranges)

    # importing the csv file as a Pandas df with the filtered data from the NIST Database
    nist_df = pd.read_csv('data/x_nist.csv')
    print(nist_df.replace("NaN", np.nan))
    nist_df = nist_df.replace("NaN", np.nan).dropna(subset=['obs_wl'])
    nist_df = f.set_dtypes(nist_df)
    print(nist_df)
    
    min_ion = f.find_min_ion(nist_df)
    print(min_ion)
