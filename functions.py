# This file contains all important functions to be used in main.py
from pathlib import Path
import csv

import numpy as np
import pandas as pd

def find_fnames():
    """
    looks for files in csv format within the correct path and 
    returns a list of the given filenames.

    Returns
    -------
    list of filenames : list
        list of strings with suffixes of filenames.

    """

    path = Path('data/{fname}.csv' .format(fname='foo'))
    return sorted([fname.stem for fname in path.parent.glob('*.csv')])

def read_data(fname):
    """
    reads the csv data exported from Matlab and returns np.arrays
    for each of the files.
    
    Parameters
    ----------
    fname : str
        this is the filename suffix.

    Returns
    -------
    data : np.array
        returns the np.array of the given file. It can return a ndim array.
    """

    fpath = 'data/{fname}.csv' .format(fname = fname)
    with open(fpath, 'r') as f:
        reader = csv.reader(f)
        next(reader) # skip the header row
        data = np.array([row for row in reader], dtype=float)

    return data


def read_data2(fname):
    """
    reads the data provided in csv format and returns a list of datasets
    for each of the files.
    
    Parameters
    ----------
    fname : str
        this is the filename suffix.

    Returns
    -------
    data : np.array
        returns a list of np.arrays for the given file.
    """
    fpath = 'data/{fname}.csv'.format(fname=fname)
    datasets = []  # create an empty list to store the datasets
    with open(fpath, 'r', newline='') as f:
        reader = csv.reader(f)
        # Loop over each row in the CSV file
        for i, row in enumerate(reader):
            # Convert the row to a NumPy array and append it to the list
            datasets.append(np.array([float(val) for val in row]))
    return datasets

def calculate_snr(noise_signal, sp_lines):
    """
    calculates the SNR of each spectral line.
    
    Parameters
    ----------
    noise_signal : np.array
        this is noise signal
    sp_lines : list
        these are the true spectral lines

    Returns
    -------
    snr_values : list
        returns a list with the snr values of each signal
    """

    noise_mean = np.mean(noise_signal)
    noise_std = np.std(noise_signal)

    snr_values = []
    for line in sp_lines:
        # calculate RMS value and SNR for each signal
        signal_rms = np.sqrt(np.mean((line - noise_mean)**2))
        signal_snr = signal_rms / noise_std
        snr_values.append(signal_snr)

    return snr_values

def normalize_sp(wavelengths, spectrum, deg):
    """
    calculates the normalized spectra using polyfit.
    
    Parameters
    ----------
    frequency : np.array
        this is the wavelength of the given spectral line.

    Returns
    -------
    data : np.array
        returns a list of np.arrays for the given file.
    """
    coeffs = np.polyfit(wavelengths, spectrum, deg)
    continuum = np.polyval(coeffs, wavelengths)
    # Divide the spectrum by the continuum to obtain a normalized spectrum
    return spectrum / continuum

def filter_abs_lines(wl_range, pf_lines):
    """
    filters every absorbtion line for later plotting.
    
    Parameters
    ----------
    wl_range : np.array
        this is the wavelength range for all lines.
    pf_lines : list
        this is the list of polyfitted lines to filter out.

    Returns
    -------
    filt_wl_range : np.array
        returns a list of np.arrays for the given file.
    abs_lines : list
        returns a list of lists of np.arrays for each range.
    """
    range1 = slice(1238,1268)
    range2 = slice(1290,1320)
    range3 = slice(1345,1375)
    range4 = slice(1400,1430)
    filt_wl_range = [wl_range[range1], wl_range[range2], wl_range[range3], wl_range[range4]]
    abs_line1 = []
    abs_line2 = []
    abs_line3 = []
    abs_line4 = []
    for line in pf_lines:
        abs_line1.append(line[range1])
        abs_line2.append(line[range2])
        abs_line3.append(line[range3])
        abs_line4.append(line[range4])
    abs_lines = [abs_line1, abs_line2, abs_line3, abs_line4]
    return filt_wl_range, abs_lines

import numpy as np

def is_periodic(signal):
    """
    finds if the signal is periodic using the autocorrelation function.
    
    Parameters
    ----------
    signal : np.array)
        The input signal
    
    Returns
    prints wether it is periodic or not. if it is periodic, prints its period.
    """
    corr = np.correlate(signal, signal, mode='full')
    max_idx = np.argmax(corr)
    period = max_idx - len(signal)

    if period == 0:
        return print("The signal is not periodic")
    else:
        return print("The signal is periodic with period", period)

def is_power_signal(signal):
    """
    Determines whether the input signal is a power signal.

    Parameters:
    -----------
    signal: numpy array
        The input signal to be checked.

    Returns:
    --------
    is_power: boolean
        True if the input signal is a power signal, False otherwise.
    """

    # Calculate the mean squared value of the signal
    mean_squared_value = np.mean(np.square(signal))

    # Check if the mean squared value is finite and nonzero
    
    is_power = np.isfinite(mean_squared_value) and mean_squared_value > 0
    if is_power == True:
        return print(f"The signal is not a power signal. Its RMS is {mean_squared_value}")
    else:
        return print(f"The signal is an energy signal. Its RMS is {mean_squared_value}")
    
def set_dtypes(df):
    df['obs_wl'] = df['obs_wl'].astype('float')
    df['uncertainty'] = df['uncertainty'].astype('float')
    df['peak_value'] = df['peak_value'].astype('float')
    df['lower_range'] = df['lower_range'].astype('float')
    df['upper_range'] = df['upper_range'].astype('float')
    df['corr_line'] = df['corr_line'].astype('category')
    return df
    
def find_min_ion(df):

    # calculating error
    df['error'] = df['peak_value'] - df['obs_wl']
    
    # grouping by 'ion' and 'corr_line'
    grouped = df.groupby(['ion', 'corr_line'])
    
    # creating a new dataframe with the errors for each 'ion' and 'corr_line'
    errors = pd.DataFrame(grouped['error'].apply(list))
    
    # creating a new dataframe with the minimum error for each 'ion' and 'corr_line'
    min_errors = pd.DataFrame(grouped['error'].min())
    
    # finding the two elements in 'ion' with the smallest error in each 'corr_line'
    result = pd.DataFrame()
    for corr_line in ['first', 'second', 'third', 'fourth']:
        temp_df = min_errors.loc[min_errors.index.get_level_values('corr_line') == corr_line]
        sorted_df = temp_df.sort_values(by='error', ascending=True)
        result = pd.concat([result, sorted_df.head(3)])
    
    return result
